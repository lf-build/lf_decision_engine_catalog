declare var module;
declare function require(name:string);

var Router = require('koa-router');
var bodyParser = require('co-body');
var request = require('co-request');

var test = function *() {
    var posts = yield request({
        uri: 'http://jsonplaceholder.typicode.com/posts/1',
        method: 'GET'
    });
    var res = posts.body;
    for (var i of test()) {
        return yield i;
    }
    yield i;
};

var testParse = function *() {
    var res = yield* test();
    console.log(res.body);
    this.body = res;
}

// Get the index of group end
var getEndGroupIndex = function(startIndex, groupStr, groupType) {
    var parensStartCount = 0;
    var parensEndCount = 0;
    
    groupType = groupType.split('');
    for (var i = startIndex; i < groupStr.length; i++) {
        var element = groupStr[i];
        
        if (element === groupType[0]) {
            parensStartCount++;
        }
        if (element === groupType[1]) {
            parensEndCount++;
        }
        if (parensEndCount === parensStartCount && parensEndCount !== 0 && parensStartCount !== 0) {
            return callEndIndex = i;
        }
    }
}

// Converts all calls to ES6 compatible code
var parseRulesAndFunctions = function *(aStr, start) {
    var res;
    start = start || 0;
    
    var fnDetails = getFnDetails(aStr, start);
    
    // Construct parsed string for functions
    if (fnDetails.fnType === 'call') {
        res = aStr.slice(0, fnDetails.startIndex) + fnDetails.fnName + '.apply(null, ' + fnDetails.fnParams + aStr.slice(fnDetails.endIndex);
    }
    
    // Construct parsed string for rules
    if (fnDetails.fnType === 'run') {
        var url = yield request('http://jsonplaceholder.typicode.com/posts/1');
        console.log(url);
        res = aStr.slice(0, fnDetails.startIndex) + 'yield request(' + fnDetails.fnName + ',' + fnDetails.fnParams + aStr.slice(fnDetails.endIndex);
    }
    
    // Recurse if there are more calls
    // Set startIndex to last parsed index to reduce time complexity to O(logn)
    if (res.indexOf('call') !== -1 || res.indexOf('run') !== -1) {
        for (var i of parseRulesAndFunctions(res, fnDetails.paramEnd)) {
            if (i.indexOf && i.indexOf('call') === -1 && i.indexOf('run') === -1) {
                return i;
            }
        }
    }
    yield res;
}

// Collect function details
var getFnDetails = function(aStr, start) {
    // Determine whether to parse for run or call
    var callIndex = aStr.indexOf('call(', start);
    var runIndex = aStr.indexOf('run(', start);
    var startIndex;
    var fnType;
    
    // Parse for callIndex
    if (runIndex === -1 || callIndex < runIndex) {
        fnType = 'call'
        startIndex = aStr.indexOf('call(', start);
    }
    // Parse for runIndex
    if (callIndex === -1 || runIndex < callIndex) {
        fnType = 'run'
        startIndex = aStr.indexOf('run(', start);
    }    
    
    // Get definition
    var endIndex = getEndGroupIndex(startIndex, aStr, '()');
    var fnDefinition = aStr.slice(aStr.indexOf('(', startIndex) + 1, endIndex + 1);
    
    // Get name and parameters
    var fnName = fnDefinition.split(',')[0].replace(/"|'/g, "");
    var paramStart = fnDefinition.indexOf('[');
    var paramEnd = getEndGroupIndex(paramStart, fnDefinition, '[]');
    var fnParams = fnDefinition.slice(paramStart, paramEnd + 1);
    
    var res = {
        fnName: fnName,
        fnParams: fnParams,
        startIndex: startIndex,
        endIndex: endIndex,
        paramEnd: paramEnd,
        fnType: fnType
    };
    
    return res;
}

// Runes code through parser functions
var parseUserExpression = function *() {
    var body = yield bodyParser.json(this);
    
    var fnString = body.ts;
    
    var res = fnString;
    res = yield* parseRulesAndFunctions(res);
    
    this.body = res;
};

var register = function(app) {
    var router = new Router({
        prefix: '/parse'
    });
    router.get('/', testParse);
    router.post('/', parseUserExpression);
    app.use(router.routes()).use(router.allowedMethods());
}

module.exports = register;