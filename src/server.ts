var cors = require('koa-cors');
var koa = require('koa');
var app = koa();

require('./endpoints/parser')(app);

app.use(cors());

var port = process.env.PORT || 1337

app.listen(port);
console.log('listening on port ', port);