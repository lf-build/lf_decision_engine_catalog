var gulp = require('gulp');
var shell = require('gulp-shell');
var ts = require('gulp-typescript');
var watch = require('gulp-watch');

var tsProject = ts.createProject('tsconfig.json');
    
gulp.task('default', ['build', 'watch']);

gulp.task('build', function() {
    return gulp.src('src/**/*.ts')
        .pipe(ts(tsProject))
        .pipe(gulp.dest('app/'));
});

gulp.task('watch', function() {
    gulp.watch('**/*.ts', ['build']);
})

gulp.task('serve', function() {
        gulp.src('', {read: false})
        .pipe(shell([
            "nodemon --harmony app/server.js"
        ]));
})